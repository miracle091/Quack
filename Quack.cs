using ff14bot;
using ff14bot.Behavior;
using ff14bot.Helpers;
using ff14bot.Interfaces;
using ff14bot.Managers;
using Newtonsoft.Json;
using Quack.Settings;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace Quack_
{
	public class Quack : IBotPlugin
	{
		#region Necessary Stuff

		public string Author => "Exmortem";
		public string Description => "Logs out based on settings.";
		public Version Version => new Version(0, 0, 1);

		public string Name => "Quack!";
		public bool WantButton => true;
		public string ButtonText => "Quack!";

		public void OnInitialize() => settings.Enabled = false;
		public void OnEnabled() => settings.Enabled = false;

		public bool Equals(IBotPlugin other)
		{
			throw new NotImplementedException();
		}

		public void OnShutdown()
		{
		}

		public void OnDisabled()
		{
		}

		public void OnButtonPress()
		{
			if (_form == null || _form.IsDisposed || _form.Disposing) _form = new frmMain();
			try
			{
				_form.Show();
				_form.Activate();
			}
			catch (ArgumentOutOfRangeException e) { }
		}

		#endregion Necessary Stuff

		#region Variables

		public static QuackSettings settings = QuackSettings.Instance;
		private frmMain _form;

		public static string[] Hours = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
		public static string[] Minutes = { "0", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55" };
		public static string[] MainCities = { "New Gridania", "Limsa Lominsa", "Ul'dah", "Foundation", "Idyllshire", "Mor Dhona", "Rhalgr's Reach", "Kugane" };

		#endregion Variables

		public void OnPulse()
		{
			if (settings.Enabled)
			{
				if (System.DateTime.Now >= settings.TimeEnd)
				{
					if (Core.Player.InCombat)
					{
						Logging.Write("[Quack!] Waiting for combat to end.");
						return;
					}
					else
					{
						if (settings.Teleport)
						{
							if (Core.Player.IsMounted)
							{
								Logging.Write("[Quack!] Mounted, attempting dismount.");
								Thread.Sleep(1000);
								ActionManager.Dismount();
								Thread.Sleep(1000);
							}
							CommonBehaviors.MoveStop();
							Logging.Write("[Quack!] Teleporting to {0}.", settings.TeleportLocation);
							switch (settings.TeleportLocation)
							{
								case "New Gridania":
									WorldManager.TeleportById(2);
									break;

								case "Limsa Lominsa":
									WorldManager.TeleportById(8);
									break;

								case "Ul'dah":
									WorldManager.TeleportById(9);
									break;

								case "Foundation":
									WorldManager.TeleportById(70);
									break;

								case "Idyllshire":
									WorldManager.TeleportById(75);
									break;

								case "Mor Dhona":
									WorldManager.TeleportById(24);
									break;

								case "Rhalgr's Reach":
									WorldManager.TeleportById(104);
									break;

								case "Kugane":
									WorldManager.TeleportById(111);
									break;

								default:
									Logging.Write("[Quack!] Can't teleport to {0}.", settings.TeleportLocation);
									break;
							}
							Thread.Sleep(20000);
						}

						Logging.Write("[Quack!] Stopping Bot!");
						TreeRoot.Stop();

						if (settings.Logout)
						{
							Logging.Write("[Quack!] Closing FFXIV.");
							Process ffxiv = Core.Memory.Process;
							ffxiv.Kill();
						}
					}
				}
			}
		}
	}
}

#region Settings

namespace Quack.Settings
{
	public class QuackSettings : JsonSettings
	{
		[JsonIgnore]
		private static QuackSettings _instance;

		public static QuackSettings Instance { get { return _instance ?? (_instance = new QuackSettings("QuackSettings")); } }

		public QuackSettings(string filename) : base(Path.Combine(CharacterSettingsDirectory, "Quack.json"))
		{
		}

		[Setting]
		public bool Enabled { get; set; }

		#region Time Settings

		[Setting]
		public int HoursElapsed { get; set; }

		[Setting]
		public int MinutesElapsed { get; set; }

		[Setting]
		public DateTime TimeEnd { get; set; }

		[Setting]
		public DateTime TimeStart { get; set; }

		#endregion Time Settings

		#region Action Settings

		[Setting]
		public bool Teleport { get; set; }

		[Setting]
		public string TeleportLocation { get; set; }

		[Setting]
		public bool Logout { get; set; }

		#endregion Action Settings
	}
}

#endregion Settings
